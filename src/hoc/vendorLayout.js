import React from "react";
const vendorLayout = (ChildComponent) => {
    class VendorLayout extends React.Component {
        constructor(props){
            super(props);
    
            this.state = {};
        }

        render(){
            return <>
            <section className="vh-100">
                <div className="container-fluid h-custom">
                    <div className="bg-white row d-flex justify-content-center align-items-center h-100">
                        <div className="col-md-8">
                            <ChildComponent {...this.props} />
                        </div>
                    </div>
                </div>
                
            </section>
        </>
        }
    }

    return VendorLayout;
}

export default vendorLayout;