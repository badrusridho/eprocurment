import React from "react";
import "../../assets/css/profile.css"
import userProfileLayout from "../../hoc/userProfileLayout";

class ProfilePage extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            currentpassword:'',
            newpassword:'',
            confirmpassword:'',
            alertSucces: false,
            alermessage: '',
            bgalert: 'bg-primary',
            btnalert: 'btn-primary',
            typepass:'password',
            typepass2:'password',
            typepass3:'password'
        };
        this.showpass=this.showpass.bind(this);
        this.showpass2=this.showpass2.bind(this);
        this.showpass3=this.showpass3.bind(this);
    }

    handleUpdate(event){
        event.preventDefault();
            const newpassword = this.state.newpassword;
            const confirmpassword = this.state.confirmpassword;
            const currentpassword = this.state.currentpassword;
            const usToken = localStorage.getItem('UsToken'); 

            if(newpassword !== confirmpassword){
                // alert("New Password and Confirm Password Not Match");
                this.setState({
                    alertSucces : true,
                    bgalert: "bg-warning",
                    btnalert: "btn-warning"
                })  
                this.setState({
                    alertSucces : true,
                    alermessage: "New Password and Confirm Password Not Match"
                })  
            }else{
                fetch("https://admin.delpis.online/api/users/updatenewpass", {
                    method: "post",
                    body: JSON.stringify({
                        password: currentpassword,
                        newpassword: newpassword,
                        token: usToken,
                        user_name: ""
                    }),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                }).then(response => response.json())
                .then((response) => {
                    // this.UserList();
                    // console.log(response['token']);
                    if(response['status'] === false){
                        // alert(response['message']);
                        this.setState({
                            alertSucces : true,
                            bgalert: "bg-danger",
                            btnalert: "btn-danger"
                        })  
                        this.setState({
                            alertSucces : true,
                            alermessage: response['message']
                        })  
                    }else{
                        // alert("Password Has Been Changed");
                        this.setState({
                            alertSucces : true,
                            bgalert: "bg-success",
                            btnalert: "btn-success"
                        })  
                        this.setState({
                            alertSucces : true,
                            alermessage: "Password Has Been Changed"
                        })  
                        window.location = "/";
                        // this.props.history.push("/home");
                        // console.log("Request succeeded with  response");
                    }
                })
                .catch(function(error) {
                    // this.setState({isLogin : ""});
                    console.log("Request failed", error);
                });
            }
    }

    showpass(){
        if(this.state.typepass == "password"){
            this.setState({typepass : 'text'})
        }else{
            this.setState({typepass : 'password'})
        }
    }

    showpass2(){
        if(this.state.typepass2 == "password"){
            this.setState({typepass2 : 'text'})
        }else{
            this.setState({typepass2 : 'password'})
        }
    }

    showpass3(){
        if(this.state.typepass3 == "password"){
            this.setState({typepass3 : 'text'})
        }else{
            this.setState({typepass3 : 'password'})
        }
    }

    render(){
        return <>
                <div className="my-3 p-3 bg-body rounded shadow-sm">
                    <h6 className="border-bottom pb-2 mb-0 mb-3">Change Password</h6>

                    <div className="row">
                        <div className="col-8">
                            <form>
                                <div className="mb-3">
                                    <label htmlFor="exampleInputEmail1" className="form-label">Current Password</label>
                                    <div className="input-group mb-3">
                                        <input type={this.state.typepass} className="form-control" placeholder="Current Password" aria-label="Recipient's username" aria-describedby="basic-addon2"
                                        onChange={(e) => this.setState({currentpassword : e.target.value})}/>
                                        <span className="input-group-text" id="basic-addon2"><i className={`fa fa-eye ` + (this.state.typepass === 'password' ? `fa-eye-slash` : `fa-eye`)}
                                        onClick={this.showpass}></i></span>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="exampleInputEmail1" className="form-label">New Password</label>
                                    <div className="input-group mb-3">
                                        <input type={this.state.typepass2} className="form-control" placeholder="New Password" aria-label="Recipient's username" aria-describedby="basic-addon2"
                                        onChange={(e) => this.setState({newpassword : e.target.value})}/>
                                        <span className="input-group-text" id="basic-addon2"><i className={`fa fa-eye ` + (this.state.typepass2 === 'password' ? `fa-eye-slash` : `fa-eye`)}
                                        onClick={this.showpass2}></i></span>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="exampleInputEmail1" className="form-label">Confirm New Password</label>
                                    <div className="input-group mb-3">
                                        <input type={this.state.typepass3} className="form-control" placeholder="Confirm New Password" aria-label="Recipient's username" aria-describedby="basic-addon2"
                                        onChange={(e) => this.setState({confirmpassword : e.target.value})}/>
                                        <span className="input-group-text" id="basic-addon2"><i className={`fa fa-eye ` + (this.state.typepass3 === 'password' ? `fa-eye-slash` : `fa-eye`)}
                                        onClick={this.showpass3}></i></span>
                                    </div>
                                </div>
                                {/* <hr/> */}
                                <button type="button" className="btn btn-default"
                                onClick={this.handleUpdate.bind(this)}>Update Password</button>
                            </form>
                        </div>
                    </div>
                </div>

                {this.state.alertSucces && 
                <div className="ModalAlert">
                    <div className="alertNew">
                        <div className="modal-content">                   
                            <div className={`modal-header ${this.state.bgalert}`}>
                                <h5 className="modal-title" id="exampleModalLabel">Alert Message</h5>
                                <button type="button" className="btn-close" aria-label="Close"
                                    onClick={(e) => this.setState({alertSucces : false})}>
                                    <i className="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div className="modal-body text-black">
                                <center><h1>{ this.state.alermessage }</h1></center>
                            </div>            
                            <div style={{width:"100%"}}>
                                <center>
                                <button className={`btn ${this.state.btnalert}`} onClick={(e) => this.setState({alertSucces : false})}>
                                    OK</button> 
                                </center>
                            </div>
                            <br></br>
                        </div>
                    </div>
                </div>
                }
        </>
    }
}

export default userProfileLayout(ProfilePage);