import React from "react";
import { Link } from 'react-router-dom';
import regisvendorLayout from "../RegisVendorLayout";
import ModalComponent from "../../../components/ModalComponent";

class PengurusVendorPage extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            pengName:'',
            pengJabatan:'',
            pengKTP:'',
            pengNPWP:'',
            manage:[],
            id:'',
            alertSucces: false,
            alermessage: '',
            bgalert: 'bg-primary',
            btnalert: 'btn-primary',
            typepass:'password'
        };
        this.handleSave = this.handleSave.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.onNPWP = this.onNPWP.bind(this);
        this.onKTP = this.onKTP.bind(this);
    }
    
    componentDidMount() {
        this.ManageVendor();
    }
      
    ManageVendor() {           
        const usToken = localStorage.getItem('UsToken'); 
        fetch("https://vendor.delpis.online/api/vendor/management", {
            method: "post",
            body: JSON.stringify({
                token: usToken
            }),
            headers:{
                'Content-Type': 'application/json'
            }
        })
        .then((response) => response.json())
        .then((response) => {
            this.setState({
                manage: response['managementData'] || []
            });
            
            localStorage.setItem("ManageVendor", JSON.stringify(this.state.manage));
        })
    }

    adddata(){
        this.setState({
            pengName:'',
            pengJabatan:'',
            pengKTP:'',
            pengNPWP:'',
        })
    }

    viewedit(item){
        this.setState({
            pengName: item.fullname || '',
            pengJabatan: item.position || '',
            pengKTP: item.idcard || '',
            pengNPWP: item.npwp || '',
            id: item.idcard || '',
            editsts:1
        })
    }

    delete(KTPpeng){
        this.setState({
            id : KTPpeng
        })  
        // this.setState({groupuserdata})
    }

    handleDelete(event){
        const data = this.state.manage.filter(i => i.idcard !== this.state.id)
        this.state.manage = data;
        localStorage.setItem("ManageVendor", JSON.stringify(this.state.manage));
        this.setState({
            person : data,
            alertSucces : true,
            alermessage: "Deleted Succes",
            bgalert: "bg-danger",
            btnalert: "btn-danger"})
    }

    handleSave(event){
        event.preventDefault();
        this.setState({
            bgalert: "bg-warning",
            btnalert: "btn-warning"
        })

        if(this.state.pengName == "" || this.state.pengName == null){
            this.setState({
                alertSucces : true,
                alermessage: "Nama harus di isi"
            })  
            return;
        }

        if(this.state.pengJabatan == "" || this.state.pengJabatan == null){
            this.setState({
                alertSucces : true,
                alermessage: "Jabatan harus di isi"
            })  
            return;
        }

        if(this.state.pengKTP == "" || this.state.pengKTP == null){
            this.setState({
                alertSucces : true,
                alermessage: "No. KTP harus di isi"
            })  
            return;
        }

        if(this.state.pengNPWP == "" || this.state.pengNPWP == null){
            this.setState({
                alertSucces : true,
                alermessage: "No. NPWP harus di isi"
            })  
            return;
        }
        
        if(this.state.editsts === 1){
            const newData = Object.values(this.state.manage);

            const index = newData.findIndex(item => item.idcard === this.state.id);
            console.log('index : ' + index);
            // Lakukan perubahan pada salinan array
            if (index !== -1) {
                newData[index] = { ...newData[index],
                    fullname: this.state.pengName,
                    idcard: this.state.pengKTP,
                    position: this.state.pengJabatan,
                    npwp: this.state.pengNPWP
                };
                // console.log(newData);
                this.state.manage = newData; // Update state dengan array yang sudah diubah
                localStorage.setItem("ManageVendor", JSON.stringify(this.state.manage));
            }
        } else {        
            const oldArray = Object.values(this.state.manage);
            var newStateArray = oldArray.slice();
            newStateArray.push({
                "fullname": this.state.pengName,
                "idcard": this.state.pengKTP,
                "position": this.state.pengJabatan,
                "npwp": this.state.pengNPWP
                });
            this.state.manage = newStateArray;

            localStorage.setItem("ManageVendor", JSON.stringify(this.state.manage));
        }
        this.TombolElement.click();
    }
   
    onKTP(e){
       const re = /^[0-9\b]+$/;
       if (e.target.value === '' || re.test(e.target.value)) {
          this.setState({pengKTP: e.target.value})
       }
    }
   
    onNPWP(e){
       const re = /^[0-9\b]+$/;
       if (e.target.value === '' || re.test(e.target.value)) {
          this.setState({pengNPWP: e.target.value})
       }
    }
    
    modalFooterDeleted(){      

        return <>
            <div style={{width:"100%"}}>
                <button className="btn btn-success" onClick={this.handleDelete}
                    data-bs-dismiss="modal">Yes</button>
            &nbsp;
            <button className="btn btn-danger"
                data-bs-dismiss="modal">No</button>
            </div>
        </>;
    }

    modalDeleted(){
        return <>
            <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabIndex="0">
                <form>
                    <div className="mb-3">
                        <label htmlFor="group_name" className="form-label">Apakah anda yakin ingin menghapus data?</label>
                    </div>
                </form>
            </div>
        </>;
    }

    modalFooterContent(){
        return <>
            <div style={{width:"100%"}}>
                <button className="btn btn-success" onClick={this.handleSave}>Save</button> 
                &nbsp;
                <button className="btn btn-danger" ref={tombol => this.TombolElement = tombol}
                    data-bs-dismiss="modal">Cancel</button> 
            </div>
        </>;
    }

    modalContent(){
        return <>
            {/* <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li className="nav-item" role="presentation">
                    <button className="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Group User Data</button>
                </li>
            </ul> */}
            <div className="tab-content" id="pills-tabContent">
                <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabIndex="0">
                    <form>
                        <div className="mb-3">
                            <label htmlFor="PengurusName" className="form-label">Nama <span className="symbol required" style={{color:'red'}}>*</span></label>
                            <input type="text" className="form-control" id="PengurusName" aria-describedby="Group_name"
                                onChange={(e) => this.setState({pengName : e.target.value})}
                                value={ this.state.pengName }/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="PengurusJabatan" className="form-label">Jabatan <span class="symbol required" style={{color:'red'}}>*</span></label>
                            <input type="text" className="form-control" id="PengurusJabatan" aria-describedby="Group_name"
                                onChange={(e) => this.setState({pengJabatan : e.target.value})}
                                value={ this.state.pengJabatan }/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="PengurusKTP" className="form-label">KTP <span class="symbol required" style={{color:'red'}}>*</span></label>
                            <input type="text" className="form-control" id="PengurusKTP" aria-describedby="Group_name"
                                onChange={this.onKTP} maxLength={16}
                                value={ this.state.pengKTP }/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="PengurusNPWP" className="form-label">NPWP <span class="symbol required" style={{color:'red'}}>*</span></label>
                            <input type="text" className="form-control" id="PengurusNPWP" aria-describedby="Group_name"
                                onChange={this.onNPWP} maxLength={16}
                                value={ this.state.pengNPWP }/>
                        </div>
                    </form>
                </div>
            </div>
        </>;
    }

    render(){
        var no = 1;

        const VendorManage = JSON.parse(localStorage.getItem('ManageVendor'));
        // const VendorContact = localStorage.getItem('ContactVendor');
        if(VendorManage){
            this.state.manage = VendorManage;
        }
        // console.log(this.state.contact);
        const ManageVendor = this.state.manage.map((item, i) => (

            <tr>
                <td>{ no++ }</td>
                <td>{ item.fullname }</td>
                <td>{ item.position }</td>  
                <td>{ item.idcard }</td>   
                <td>{ item.npwp }</td>      
                <td>
                    <div className="dropdown table-action-dropdown">
                        <button className="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButtonSM" data-bs-toggle="dropdown" aria-expanded="false"><i className="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                        <ul className="dropdown-menu dropdown-prof" aria-labelledby="dropdownMenuButtonSM">
                            <li><a className="dropdown-item" href="#" onClick={this.viewedit.bind(this, item)}
                                data-bs-toggle="modal" data-bs-target="#exampleModalDefault">
                                <i className="fa fa-pencil" aria-hidden="true"></i>&nbsp;Edit</a></li>
                            <div className="dropdown-divider"></div>        
                            <li><a className="dropdown-item text-danger" href="#" onClick={this.delete.bind(this, item.idcard)}
                                data-bs-toggle="modal" data-bs-target="#deletedModal">
                                <i className="fa fa-trash" aria-hidden="true"></i>&nbsp;Delete</a></li>                            
                        </ul>
                    </div>
                </td>
            </tr>
        ));

        return <>
            <div>
                <div className="row bg-primary text-white">
                    <center><h1>Pengurus Perusahaan</h1></center>
                </div>
                <div className="row">
                    &nbsp;
                </div>
                <div className="row">
                    <div className="col-6">
                        <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalDefault"
                            onClick={this.adddata.bind(this)}>
                            Add
                        </button>
                    </div>
                </div>

                <div className="row">
                    &nbsp;
                </div>

                <div className="row">                    
                    <table className="table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th className="Col-klik">Nama</th>
                                <th className="Col-klik">Jabatan</th>
                                <th className="Col-klik">KTP</th>
                                <th className="Col-klik">NPWP</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            { ManageVendor }
                        </tbody>
                    </table>
                </div>

                <div className="text-right text-lg-start mt-4 pt-2">
                    <Link to="/RegisVendorSPT" type="button" className="btn btn-primary btn-lg">Next</Link>
                </div>
            </div>
                
            <ModalComponent title="Deleted Confirmation" footerContent={this.modalFooterDeleted()} content={this.modalDeleted()} dataBsBackdrop="static" id="deletedModal"/>
            <ModalComponent title="Add Pengurus Perusahaan" footerContent={this.modalFooterContent()} content={this.modalContent()} dataBsBackdrop="static" id="exampleModalDefault"/>
                

            {this.state.alertSucces && 
                <div className="ModalAlert">
                    <div className="alertNew">
                        <div className="modal-content">                   
                            <div className={`modal-header ${this.state.bgalert}`}>
                                <h5 className="modal-title" id="exampleModalLabel">Alert Message</h5>
                                <button type="button" className="btn-close" aria-label="Close"
                                    onClick={(e) => this.setState({alertSucces : false})}>
                                    <i className="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div className="modal-body text-black">
                                <center><h1>{ this.state.alermessage }</h1></center>
                            </div>            
                            <div style={{width:"100%"}}>
                                <center>
                                <button className={`btn ${this.state.btnalert}`} onClick={(e) => this.setState({alertSucces : false})}>
                                    OK</button> 
                                </center>
                            </div>
                            <br></br>
                        </div>
                    </div>
                </div>
                }
        </>
    }
}

export default regisvendorLayout(PengurusVendorPage);